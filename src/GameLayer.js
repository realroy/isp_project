MODE = {
  VS_COM: 1,
  VS_FRIEND: 0
};

GAME = {
    IS_RUNNING: 1,
    STOPPED: 0
};

timer = 0;
TIME_PER_ROUND = 120;

score = {
    red: 0,
    blue: 0,
};

KEY = {
    W: 87,
    S: 83,
    A: 65,
    D: 68,
    R: 82,
};

var GameLayer = cc.LayerColor.extend({
  ctor: function( mode ) {
    this._super( new cc.Color( 127, 127, 127, 255 ) );
    this.setPosition( new cc.Point( 0, 0 ) );
    this.mode = mode;
    this.addKeyboardHandlers();
    this.status = GAME.IS_RUNNING;
    this.checkGameStatus();

    this.bg = new cc.Sprite( res.ground_png );
    this.bg.setPosition(160,200);
    this.addChild( this.bg );

    this.red = new Red();
    this.addChild( this.red );
    this.red.scheduleUpdate();

    if( this.mode == MODE.VS_COM ) {
      this.blue = new Bot();
    }
    else if(this.mode == MODE.VS_FRIEND) {
      this.blue = new Blue();
    }

    this.addChild( this.blue );
    this.blue.scheduleUpdate();

    this.pig = new Pig();
    this.addChild( this.pig );
    this.pig.scheduleUpdate();

    this.redScoreLabel = cc.LabelTTF.create('0', 'Arial', 40);
    this.redScoreLabel.setPosition(new cc.Point(730, 575));
    this.addChild( this.redScoreLabel );

    this.timerLabel = cc.LabelTTF.create('0', 'Arial' , 40);
    this.timerLabel.setPosition(new cc.Point(SCREEN.WIDTH / 2, 575));
    this.addChild( this.timerLabel );

    this.blueScoreLabel = cc.LabelTTF.create( '0', 'Arial', 40 );
    this.blueScoreLabel.setPosition( new cc.Point( 80, 575 ) );
    this.blueScoreLabel.setString( "Blue: " + score.blue );
    this.addChild( this.blueScoreLabel );

    this.winLabel = cc.LabelTTF.create( '0', 'Arial', 100 );
    this.winLabel.setPosition(new cc.Point(SCREEN.WIDTH / 2, SCREEN.HEIGHT / 2));

    this.replayLabel = cc.LabelTTF.create( '0', 'Arial', 40 );
    this.replayLabel.setPosition(new cc.Point(SCREEN.WIDTH / 2, (SCREEN.HEIGHT / 2)-100));

    this.redFlag = new cc.Sprite( res.redFlag_png );
    this.redFlag.setPosition( 780, SCREEN.HEIGHT / 2);
    this.addChild( this.redFlag );

    this.blueFlag = new cc.Sprite( res.blueFlag_png );
    this.blueFlag.setPosition( 20, SCREEN.HEIGHT / 2 );
    this.addChild( this.blueFlag );

    this.init();
  },

  init: function() {
    this.red.setPosition( new cc.Point( SCREEN.WIDTH / 2, SCREEN.HEIGHT / 2 ) );
    this.blue.setPosition( new cc.Point(100, 100 ) );
    this.pig.randomsPosition();
    score.red = 0;
    this.redScoreLabel.setString( "Red: " + score.red );
    timer = 0;
    this.timerLabel.setString( "Time: " + (TIME_PER_ROUND - ( timer )) );
    score.blue = 0;
    this.blueScoreLabel.setString( "Blue: " + score.blue );
  },

  addKeyboardHandlers: function() {
    var self = this;
    cc.eventManager.addListener({
      event: cc.EventListener.KEYBOARD,
      onKeyPressed : function( keyCode, event ) {
        self.onKeyDown( keyCode, event );
      },
    }, this);
  },

  onKeyDown: function( keyCode, event ) {
    this.redControl( keyCode );
    if( this.mode == MODE.VS_FRIEND ) {
      this.blueControl(keyCode);
    }
    if( this.status == GAME.STOPPED && keyCode == KEY.R ) {
      this.restartGame();
    }
  },

  checkGameStatus: function() {
    if( this.status == GAME.IS_RUNNING ) {
      this.scheduleUpdate();
    }
    else {
      this.addChild( this.winLabel );
      if( score.red > score.blue ) {
        this.winLabel.setString( "Red win!" );
      }
      else if( score.red < score.blue ) {
        this.winLabel.setString( "Blue win!" );
      }
      else {
        this.winLabel.setString( "Pig win!" );
      }
      this.replayLabel.setString( "Press R to play again" );
      this.addChild( this.replayLabel );
      this.unscheduleUpdate();
    }
  },

  closeTo: function( a, b, gap) {
    return ( Math.abs( a.getPositionX() - b.getPositionX() ) <= gap && Math.abs( a.getPositionY() - b.getPositionY() ) <= gap );
  },


  checkRoundEnd: function() {
    if( ( timer / 60 ) != TIME_PER_ROUND ) {
      this.status = GAME.IS_RUNNING;
    }
    else {
      this.status = GAME.STOPPED;
    }
  },

  getsDistance: function( init , final ) {
    var initPos = {
      x: init.getPositionX(),
      y: init.getPositionY()
    };
    var finalPos = {
      x: final.getPositionX(),
      y: final.getPositionY()
    };
    var deltaX = Math.abs( finalPos.x - initPos.x );
    var deltaY = Math.abs( finalPos.y - initPos.y );
    var result = Math.round(Math.pow(Math.sqrt( deltaX + deltaY ), 2));
    return result;
  },

  findsTheNearestPeople: function() {
    var redDistance  = this.getsDistance( this.red, this.pig );
    var blueDistance   = this.getsDistance( this.blue, this.pig );

    if( redDistance < blueDistance ) {
      return this.red;
    }
    else if( redDistance > blueDistance ) {
      return this.blue;
    }
    else {
      return this.red;
    }
  },

  update: function(dt) {
    timer += 1;
    if( timer % 60 == 0 ) {
      this.timerLabel.setString( "Time: " + ( TIME_PER_ROUND - ( timer / 60 ) ) );
      this.checkRoundEnd();
      this.checkGameStatus();
    }
    this.pig.checkGrabed( this.red, this.blue );
    var people = this.findsTheNearestPeople( this.red, this.blue );
    this.pig.run( people );
    this.gameRule();
    this.stealSystem();
    if(this.mode == MODE.VS_COM ){
      this.blue.runsTo( this.pig );
    }
  },

  getScore: function( people ) {
    if( people == PEOPLE.RED) {
      score.red++;
      this.redScoreLabel.setString("Red: " + score.red);
    }
    else if( people == PEOPLE.BLUE ) {
      score.blue++;
      this.blueScoreLabel.setString( "Blue: " + score.blue );
    }
  },

  isCloseToTheFlag: function( people ) {
    var peoplePos = { x: 0, y: 0};
    var flagPos = { x: 0, y: 0};
    var gap = 15;
    if( people == PEOPLE.RED ) {
      peoplePos.x = Math.round( this.red.getPositionX() );
      peoplePos.y = Math.round( this.red.getPositionY() );
      flagPos.x   = this.redFlag.getPositionX();
      flagPos.y   = this.redFlag.getPositionY();
    }
    else if( people == PEOPLE.BLUE ) {
      peoplePos.x = Math.round( this.blue.getPositionX() );
      peoplePos.y = Math.round( this.blue.getPositionY() );
      flagPos.x   = this.blueFlag.getPositionX();
      flagPos.y   = this.blueFlag.getPositionY();
    }
    return Math.abs( peoplePos.x - flagPos.x ) <= gap && Math.abs( peoplePos.y - flagPos.y ) <= gap;
  },

  gameRule: function() {
    if( this.pig.grabedBy == PEOPLE.RED ) {
      this.pig.setPosition( this.red.getPositionX(), this.red.getPositionY());
      this.pig.unscheduleUpdate();
      if( this.closeTo( this.red , this.redFlag , 50 ) ) {
        console.log('red getScore');
        this.getScore( PEOPLE.RED );
        this.pig.randomsPosition();
        this.pig.scheduleUpdate();
        this.pig.grabedBy = PEOPLE.NONE;
      }
    }
    if( this.pig.grabedBy == PEOPLE.BLUE ) {
      this.pig.setPosition( this.blue.getPositionX(), this.blue.getPositionY());
      this.pig.unscheduleUpdate();
      if(this.mode == MODE.VS_COM) {
        this.blue.runsTo( this.blueFlag );
      }
      if( this.closeTo( this.blue , this.blueFlag , 50 ) ) {
         //console.log('The pig caught by blue');
        this.getScore( PEOPLE.BLUE );
        this.pig.grabedBy = PEOPLE.NONE;
        this.pig.randomsPosition();
        this.pig.scheduleUpdate();
      }
    }
  },

  stealSystem: function() {
    if( this.closeTo( this.red, this.pig , 30) && this.pig.grabedBy == PEOPLE.BLUE ){
      this.pig.setPosition( this.blue.getPositionX() - 25, this.blue.getPositionY() - 50 );
      this.pig.scheduleUpdate();
      this.pig.grabedBy = PEOPLE.NONE;
    }
    if( this.closeTo( this.blue, this.pig , 30) && this.pig.grabedBy == PEOPLE.RED ){
      this.pig.setPosition( this.red.getPositionX()-25, this.red.getPositionY() - 50 );
      this.pig.scheduleUpdate();
      this.pig.grabedBy = PEOPLE.NONE;
    }
  },

  restartGame: function(){
    this.removeChild( this.winLabel );
    this.removeChild( this.replayLabel );
    this.init();
    this.status == GAME.IS_RUNNING;
    this.scheduleUpdate();
  },

  redControl: function( keyCode ) {
    if( keyCode == cc.KEY.up ) {
      this.red.movesUp();
    }
    else if( keyCode == cc.KEY.down ) {
      this.red.movesDown();
    }
    else if( keyCode == cc.KEY.left ) {
      this.red.movesLeft();
    }
    else if( keyCode == cc.KEY.right ) {
      this.red.movesRight();
    }
  },

  blueControl: function( keyCode ) {
    if( keyCode == KEY.W ) {
      this.blue.movesUp();
    }
    else if( keyCode == KEY.S ) {
      this.blue.movesDown();
    }
    else if( keyCode == KEY.A ) {
      this.blue.movesLeft();
    }
    else if( keyCode == KEY.D ) {
      this.blue.movesRight();
    }
  },
});

StartScene = cc.Scene.extend({
  ctor: function( mode ) {
    this.mode = mode
    this._super();
    this.addChild( new GameLayer( mode ) );
  },
});
