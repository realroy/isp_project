var res = {
    red_png:        "res/images/Player.png",
    pig_png:        "res/images/pig.png",
    blue_png:       "res/images/Enemy.png",
    ground_png:     "res/images/floor.jpg",
    redFlag_png:    "res/images/redFlag.png",
    blueFlag_png:   "res/images/blueFlag.png",
    logo_png:       "res/images/logo.png"
};

var g_resources = [];
for (var i in res) {
    g_resources.push(res[i]);
}
