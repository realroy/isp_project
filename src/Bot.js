DIR = {
  LEFT: 1,
  RIGHT: 2,
  UP: 3,
  DOWN: 4
};
FRICTION = 0.25;
var Bot = cc.Sprite.extend({
  ctor: function() {
    this._super();
    this.initWithFile( res.blue_png , cc.rect( 100, 0, 50, 82 ) );
    this.direction = DIR.UP;
    this.SPEED = 10;
    this.v = {
      x : 0,
      y : 0
    };
    this.HEIGHT = 60;
    this.WIDTH  = 50;
  },

  movesUp: function() {
    this.v.y = this.SPEED;
    this.direction = DIR.UP;
  },

  movesDown: function() {
    this.v.y = ( -1 ) * this.SPEED;
    this.direction = DIR.DOWN;
  },

  movesLeft: function() {
    this.v.x = ( -1 ) * this.SPEED;
    this.setFlippedX( true );
    this.direction = DIR.LEFT;
  },

  movesRight: function() {
    this.v.x = this.SPEED;
    this.setFlippedX( false );
    this.direction = DIR.RIGHT;
  },

  update: function( dt ) {
    this.addsFriction();
    this.updatesPosition();
  },

  updatesPosition: function() {
    this.checksCollision();
    this.setPosition( this.getPositionX() + this.v.x, this.getPositionY() + this.v.y);
  },

  checksCollision: function() {
    var pos = {
      x : Math.round( this.getPositionX() ),
      y : Math.round( this.getPositionY() )
    };

    if( ( pos.y >= SCREEN.HEIGHT - this.HEIGHT && this.direction != DIR.DOWN ) || ( pos.y <= this.HEIGHT && this.direction != DIR.UP ) ) {
      this.v.y = 0;
    }
    if( ( pos.x >= SCREEN.WIDTH - this.WIDTH && this.direction != DIR.LEFT ) || ( pos.x <= this.WIDTH && this.direction != DIR.RIGHT ) ) {
      this.v.x = 0;
    }
  },

  runsTo: function( target ) {
    var pos = {
      x : Math.round( this.getPositionX() ),
      y : Math.round( this.getPositionY() )
    };

    var targetPosition = {
      x : Math.round( target.getPositionX() ),
      y : Math.round( target.getPositionY() )
    };

    if( pos.y > targetPosition.y ) {
      this.movesDown();
    }
    else if( pos.y < targetPosition.y ) {
      this.movesUp();
    }

    if( pos.x > targetPosition.x ) {
      this.movesLeft();
    }
    else if( pos.x < targetPosition.x ) {
      this.movesRight();
    }
  },

  addsFriction: function() {
    if( this.v.x > 0 ) {
      this.v.x -= FRICTION;
    }
    else if( this.v.x < 0 ) {
      this.v.x += FRICTION;
    }

    if( this.v.y > 0 ) {
      this.v.y -= FRICTION;
    }
    else if( this.v.y < 0 ) {
      this.v.y += FRICTION;
    }
  },
});
