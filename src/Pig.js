PEOPLE = {
  NONE: 0,
  RED: 1,
  BLUE: -1,
};
CLOSE_TO = {
  NONE: 0,
  LEFT_EDGE: 1,
  RIGHT_EDGE: 2,
  TOP_EDGE: 3,
  BUTTOM_EDGE: 4
}

var Pig = cc.Sprite.extend ({

  ctor: function(){
    this._super();
    this.initWithFile( res.pig_png );
    this.randomsPosition();
    this.SPEED = 15;
    this.v = {
      x: 0,
      y: 0,
    };
    this.HEIGHT = 50;
    this.WIDTH  = 50;
    this.grabedBy = PEOPLE.NONE;
  },

  update: function( dt ) {
    this.updatesPosition();
  },

  checkGrabed: function( red , blue ) {
    var pigPos = {
      x : Math.round( this.getPositionX() ),
      y : Math.round( this.getPositionY() )
    };
    var redPos = {
      x: Math.round( red.getPositionX() ),
      y: Math.round( red.getPositionY() )
    };
    var bluePos = {
      x: Math.round( blue.getPositionX() ),
      y: Math.round( blue.getPositionY() )
    };
    var gap = 50;
    if( Math.abs ( redPos.x - pigPos.x) <= gap && Math.abs( redPos.y - pigPos.y ) <= gap ) {
      this.grabedBy = PEOPLE.RED;
    }
    else if( Math.abs ( bluePos.x - pigPos.x ) <= gap && Math.abs( bluePos.y - pigPos.y ) <= gap ) {
      this.grabedBy = PEOPLE.BLUE;
    }
    else {
      this.grabedBy = PEOPLE.NONE;
    }
  },

  isClosetoEdge: function() {
    var pos = {
      x : Math.round( this.getPositionX() ),
      y : Math.round( this.getPositionY() )
    };
    var result;
    if( pos.x >= SCREEN.WIDTH - this.WIDTH ) {
      result = CLOSE_TO.RIGHT_EDGE;
    }
    else if( pos.x <= this.WIDTH ) {
      result = CLOSE_TO.LEFT_EDGE;
    }
    else if( pos.y >= SCREEN.HEIGHT - this.HEIGHT ) {
      result = CLOSE_TO.TOP_EDGE;
    }
    else if( pos.y <= this.HEIGHT ) {
      result = CLOSE_TO.BUTTOM_EDGE;
    }
    else {
      result = CLOSE_TO.NONE
    }

    return result;
  },

  randomsPosition: function() {
    var range = {
      x: Math.round( Math.random() * ( SCREEN.WIDTH - 100 ) ) + 30,
      y: Math.round( Math.random() * ( SCREEN.HEIGHT - 100 ) ) + 30
    };
    this.setPosition( range.x, range.y );
  },

  movesLeft: function() {
    this.v.x = (-1) * this.SPEED;
    this.setFlippedX( true );
  },

  movesRight: function() {
    this.v.x = this.SPEED;
    this.setFlippedX( false );
  },

  movesUp: function() {
    this.v.y = this.SPEED;
  },

  movesDown: function() {
    this.v.y = (-1) * this.SPEED;
  },

  updatesPosition: function() {
    this.setPosition( this.getPositionX() + this.v.x, this.getPositionY() + this.v.y);
  },

  run: function( people ) {
    if( this.isClosetoEdge() != CLOSE_TO.NONE ) {
      this.runInTheScreen();
    }
    else {
      this.runAwayFromPeople( people );
    }
  },

  runAwayFromPeople: function( people ) {
    var pos = {
      x : Math.round( this.getPositionX() ),
      y : Math.round( this.getPositionY() )
    };
    var peoplePos = {
      x: Math.round( people.getPositionX() ),
      y: Math.round( people.getPositionY() )
    };
    var choice = Math.floor( Math.random() * 2 );

    if( peoplePos.x < pos.x && peoplePos.y > pos.y ) {
      if( choice == 1 ) {
        this.movesDown();
      }
      else {
        this.movesRight();
      }
    }
    else if( peoplePos.x < pos.x && peoplePos.y < pos.y ) {
      if( choice == 1 ) {
        this.movesUp();
      }
      else {
        this.movesRight();
      }
    }
    else if( peoplePos.x > pos.x && peoplePos.y > pos.y ) {
      if( choice == 1 ) {
        this.movesDown();
      }
      else {
        this.movesLeft();
      }
    }
    else if( peoplePos.x > pos.x && peoplePos.y < pos.y ) {
      if( choice == 1 ) {
        this.movesUp();
      }
      else {
        this.movesLeft();
      }
    }
  },

  runInTheScreen: function() {
    var isClosetoEdge = this.isClosetoEdge();
    if( isClosetoEdge == CLOSE_TO.RIGHT_EDGE ) {
      this.movesLeft();
    }
    else if( isClosetoEdge == CLOSE_TO.LEFT_EDGE ) {
      this.movesRight();
    }
    else if( isClosetoEdge == CLOSE_TO.TOP_EDGE ) {
      this.movesDown();
    }
    else if( isClosetoEdge == CLOSE_TO.BUTTOM_EDGE ) {
      this.movesUp();
    }
  },
});
