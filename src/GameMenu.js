var director = cc.Director();
var GameMenu = cc.LayerColor.extend({
  ctor: function() {
    this._super(new cc.Color(232, 44, 12, 255));
    this.init();

  },

  init: function () {
    this.logo = new cc.Sprite(res.logo_png);
    this.logo.setPosition(new cc.Point(SCREEN.WIDTH / 2, (SCREEN.HEIGHT / 2) + 200));
    this.addChild(this.logo);

    this.vsComBtn        = new cc.MenuItemFont("Vs COM", vsCom);
    this.vsFriendBtn     = new cc.MenuItemFont("Vs Friend", vsFriend);

    this.vsComBtn.setPosition(new cc.Point(SCREEN.WIDTH / 2, ((SCREEN.HEIGHT / 2) - 50)));
    this.vsFriendBtn.setPosition(new cc.Point(SCREEN.WIDTH / 2, ((SCREEN.HEIGHT / 2) - 100)));

    this.menu = new cc.Menu( this.vsComBtn, this.vsFriendBtn );
    this.menu.setPosition(0,0);
    this.addChild(this.menu);

    return true;
  },
});

var vsCom = function() {
  console.log('Game Start');
  cc.director.runScene(new StartScene(MODE.VS_COM));
};

var vsFriend = function() {
  cc.director.runScene(new StartScene(MODE.VS_FRIEND));
};

GameMenu.scene = function() {
    var scene = new cc.Scene();
    var layer = new GameMenu();
    scene.addChild(layer);
    return scene;
};
